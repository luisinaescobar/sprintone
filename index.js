const express = require('express');

//app.use(express.json());

const swaggerUi = require('swagger-ui-express');
//const swaggerDocument = require('./swagger.json');
const yaml = require('js-yaml');
const fs   = require('fs');

function loadSwaggerinfo (app){
    try {
        const doc = yaml.load(fs.readFileSync('./spec.yml', 'utf8'));
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(doc));
      } catch (e) {
        console.log(e);
      }
}
function main (){
    const app = express();
    loadSwaggerinfo(app)
    app.listen(3000, () => console.log("listening on 3000"))
}
main();
//

///// Arraay con el administrador /////
const validAdmin =[
    {"nombre": "admin", "id": 1, "admin": true}
];
function validAdmins (req, res, next){
    if (validAdmin){
        next();
    }
}

///// Array con un usuario /////
const validUsers = [{
    "usuario": "hola",
    "nombre": "luisina",
    "apellido": "escobar",
    "email": "123@gmail.com",
    "telefono": "123456",
    "direccion": "hola 123",
    "password": "987654",
    "repPasswrd": "987654",
    "id": 1,
    "admin": false
}];
///// Middleware para crear un usuario nuevo /////
function crearUsuario(req, res, next) {
    const { usuario, nombre, apellido, email, telefono, direccion, password, repPasswrd } = req.body;
    if (usuario && nombre && apellido && email && telefono && direccion && password && repPasswrd) {

        let existe = false;
        for (let usuario of validUsers) {
            if (usuario.email === email) {
                existe = true;
            }
        }
        if (existe === false) {
            if (password === repPasswrd) {
                const id = validUsers.length + 1;
                const noAdmin = false;
                const newUser = { ...req.body, id, noAdmin }
                console.log(newUser);

                validUsers.push(newUser);
                next();
            }
            else {
                res.status(405).send("El password no coincide.");
            }
        }
        else {
            res.status(406).send("El mail ya esta registrado.");
        }
    } else {
        res.status(406).send("Por favor complete todos los campos");
    }
};
///// funcion para crear usuario ////
app.post('/signup', crearUsuario, (req, res) => {

    res.send(validUsers);
    res.status(201).send('ok');
});

///// funcion para ingresar /////
app.post('/login', function loginValidation(req, res) {

    loginEmail = String(req.body.email);
    loginPassword = String(req.body.password);

    for ( const usuario of validUsers) {
        if (usuario.email === loginEmail && usuario.password === loginPassword) {
            res.status(200).send("Ingreso exitoso");
        } else if (usuario.email != loginEmail) {
            res.status(401).send("Email incorrecto");
            
        } else if (usuario.password != loginPassword) {
            res.status(406).send("Contraseña incorrecta");
            
        }
    }
})
