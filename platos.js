const express = require('express');
const app = express();
app.use(express.json());

///// array con los platos del menu /////
const platos = [
    { 'id': 1, 'descripcion': 'Bagel de salmon', 'precio': 425, 'cantidad': 1},
    { 'id': 2, 'descripcion': 'Hamburguesa clasica', 'precio': 500,'cantidad': 1 }, 
    { 'id': 3, 'descripcion': 'Ensalada Veggie', 'precio': 325, 'Cantidad': 1 }]

///// Middleware para buscar un plato especifico ////// 
function midBuscarId(req, res, next) {
        const idPlato = Number(req.params.id);
        for (const plato of platos) {
            if (plato.id === idPlato) {
                return next();
            }
        }
        res.status(404).send('plato no encontrado')
};
///// endpoint para ver los platos /////
app.get('/platos', (req, res,) => {
    res.send(platos);
});
///// funcion para crear un nuevo plato /////
app.post('/platos', function crearplato(req, res) {
    platos.push(req.body);
    res.send(platos);
});
///// Funcion para ver el plato (usando middleware)/////
app.get('/platos/:idPlato', midBuscarId, function (req, res) {
    if (req.params.id !== null && req.params.id !== undefined) {
        const idPlato = Number(req.params.id);
        for (const plato of platos) {
            if (idPlato === plato.id) {
                res.send(plato)
            };
        }
        res.send('No existe ese plato');
    } else {
        res.send('ERROR');
    }
});
///// Funcion para eliminar el (usando middleware)/////
app.delete('/platos/:idPlato', midBuscarId, function eliminar(req, res) {
    let platoId = Number(req.params.id);
    for (const plato of platos)
        if (plato.id === platoId) {
            const pos = platos.indexOf(plato)
            platos.splice(pos, 1);
            res.send('plato eliminado');
        }
});
///// Funcion para modificar el plato (usando middleware)/////
app.put('/platos/:id', midBuscarId, function modificar(req, res) {
    const platoId = Number(req.params.id);
    for (const plato of platos)
        if (plato.id === platoId) {
            const pos = platos.indexOf(plato);
            platos[pos].id = req.body.id;
            platos[pos].descripcion = req.body.descripcion;
            platos[pos].precio = req.body.precio;
            
            res.send('plato modificado');
        }
});

app.listen(3000, () => console.log("listening on 3000"));