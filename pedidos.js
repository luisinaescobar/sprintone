const express = require('express');
const app = express();
app.use(express.json());
const fs = require('fs');
const moment = require('moment');
const file = require('./file')

///// middleware para validar si es Admin /////
function validAdmins(req, res, next) {
    if (validAdmin) {
        next();
    }
};
///// middleware para ingresar a usuarios /////
function loginValidation(req, res, next) {

    loginEmail = String(req.body.email);
    loginPassword = String(req.body.password);

    for (const usuario of validUsers) {
        if (usuario.email === loginEmail && usuario.password === loginPassword) {
            res.status(200).send("Ingreso exitoso");
            next();
        } else if (usuario.email != loginEmail) {
            res.status(401).send("Email incorrecto");

        } else if (usuario.password != loginPassword) {
            res.status(406).send("Contraseña incorrecta");

        }
    }
};
///// Arrays /////
const platos = [
    { 'id': 1, 'descripcion': 'Bagel de salmon', 'precio': 425 },
    { 'id': 2, 'descripcion': 'Hamburguesa clasica', 'precio': 500 },
    { 'id': 3, 'descripcion': 'Ensalada Veggie', 'precio': 325 }]
const pagos = [
    { 'id': 1, 'descripcion': 'efectivo' },
    { 'id': 2, 'descripcion': 'tarjeta' },
    { 'id': 3, 'descripcion': 'bitcoin' }
]
const nuevosPedidos = [];
const confPedidos = [];
const estados = [
    { 'id': 1, 'descripcion': 'Pendiente' },
    { 'id': 2, 'descripcion': 'Confirmado' },
    { 'id': 3, 'descripcion': 'En preparacion' },
    { 'id': 4, 'descripcion': 'Enviado' },
    { 'id': 5, 'descripcion': 'Entregado' }];



///// endpoint para modificar estado del pedido /////
app.put('/pedidos/:idPedido', function (req, res) {
    const idPedido = Number(req.params.idPedido)
    for (const pedido of pedidos) {
        if (idPedido === pedido.id) {

            const position = pedidos.indexOf(pedido);
            pedidos[position].estado = req.body.estado;
            res.status(200).send('Se modifico el estado con exito');

        } else { res.status(406).send('Pedido no existe') }
    }
});

//falta loginValidation
///// funcion para realizar pedido /////
app.post('/pedir/:idPlato', function hacerNuevoPedido(req, res) {
    const fecha = moment().format('DD MM YYYY hh:mm');
    const idPlato = Number(req.params.idPlato);
    const cantidad = Number(req.body);

    for (const plato of platos) {
        if (idPlato === plato.id) {
            //if (cantidad > 0) {
                const subtotal = plato.precio * cantidad;
                const id = nuevosPedidos.length + 1;
                const estado = "Pendiente";
                const newPedido = { ...req.body, id, plato, fecha, estado, subtotal, };
                nuevosPedidos.push(newPedido);
                return res.json(nuevosPedidos);
            //}
            //res.status(200).send('Pedido Creado');        
        }
    } res.status(406).send('El producto no es valido');
})
/////confirmar el pedido y el pago///////
app.post('/confirm/:idPago', function hacerPedido(req, res) {
    const fecha = moment().format('DD MM YYYY hh:mm');
    const { direccion } = req.body;
    const idPago = Number(req.params.idPago);
    var total = 0
    for (nuevoP of nuevosPedidos) {
        total = (total + nuevoP.subtotal);
    };

    for (const pago of pagos) {
        if (idPago === pago.id) {

            const id = confPedidos.length + 1;
            const estado = estados[1];
            confPedidos.push(new Confirmados(id, fecha, estado, pago, total, direccion, nuevosPedidos));
            res.send(confPedidos);
            //nuevosPedidos.splice(0, nuevosPedidos.length);
        }
    } { res.status(406).send('debe seleccionar pago') }
});

/////ver el historial/////
app.get('/history', function historial(req, res) {
    res.send(confPedidos);
});
//falta agregar middelware de validAdmin
///// endpoint para ver los pedidos /////
app.get('/pedidos', function (req, res) {
    res.send(confPedidos);
});
function Confirmados(id, fecha, estado, pago, total, direccion, nuevosPedidos) {
    this.id = id;
    this.fecha = fecha;
    this.estado = estado;
    this.pago = pago;
    this.total = total;
    this.direccion = direccion;
    this.nuevosPedidos = nuevosPedidos;
}
app.get('/pago', function(req,res){
    res.send(pagos);

})
app.listen(3000, () => console.log("listening on 3000"));